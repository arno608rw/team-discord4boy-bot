# Team Discord4Boy Bot

**Team Discord4Boy Bot** is Discord bot made for Team Discord4Boy server.

## Content

- [Content](#content)
- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
  - [Windows](#windows)
  - [Unix](#unix)
- [Documentation](#documentation)
- [Templates](#templates)
  - [config.json](#configjson)
  - [database.json](#databasejson)
- [Authors](#authors)
- [Contact](#contact)
- [License](#license)

## Features

- Mining system.
- Database management.

## Requirements

- [glob](https://www.npmjs.com/package/glob)
- [node-cron](https://www.npmjs.com/package/node-cron)
- [discord.js](https://www.npmjs.com/package/discord.js)

## Installation

### Windows

* Install [Git](https://git-scm.com/download/win/).
* Install [NodeJS](https://nodejs.org/en/download/).
* Run following commands in the command prompt:
```
git clone https://gitlab.com/DeBos/team-discord4boy-bot.git
cd team-discord4boy-bot
npm install glob node-cron discord.js
cd ..
```

### Unix

* Run following commands in the terminal:
```
curl -fs https://gitlab.com/DeBos/mpt/raw/master/mpt.sh | sh -s install git nodejs
git clone https://gitlab.com/DeBos/team-discord4boy-bot.git
cd discord4boy-bot
npm install glob node-cron discord.js
cd ..
```

## Documentation

| Command                   | Description                                   |
| :------------------------ | :-------------------------------------------- |
| *ban **user** **reason**  | Bans **user** for **reason**.                 |
| *kick **user** **reason** | Kicks **user** for **reason**.                |
| *mute **user** **reason** | Mutes **user** for **reason**.                |
| *unban **user**           | Unbans **user**.                              |
| *unmute **user**          | Unmutes **user**.                             |
| *unwarn **user** **N**    | Removes **N** warnings of **user**.           |
| *warn **user** **reason** | Warns **user** for **reason**.                |
| *clear **N**              | Clears **N** messages.                        |
| *help                     | Shows help message.                           |
| *jj                       | Shows information that user is no longer AFK. |
| *money                    | Shows amount of money.                        |
| *work                     | Gives user money for work.                    |
| *zw                       | Shows information that user is AFK.           |
| *dbload                   | Loads database.                               |
| *dbsave                   | Saves database.                               |

## Templates

### config.json

```json
{
	"clientToken"        : "TOKEN",
	"ownerID"            : "ID",
	"commandPrefix"      : "!",
	"currencySymbol"     : "$",
	"entryChannelID"     : "ID",
	"exitChannelID"      : "ID",
	"workChannelID"      : "ID",
	"maxWarnings"        : 2,
	"workWaitTime"       : 3600,
	"ores"               : {
		"ore1" : {
			"chance" : 0.7,
			"value"  : 100
		},
		"ore2" : {
			"chance" : 0.3,
			"value"  : 300
		}
	},
	"autoRolesChannelID" : "ID",
	"autoRoles" : [
		{
			"emoji" : "roleID",
			"emoji" : "roleID"
		},
		{"emoji" : "roleID"}
	]
}
```

| Key                | Description                                                                                                        |
| :----------------- | :----------------------------------------------------------------------------------------------------------------- |
| clientToken        | Token of your bot. You can find it here: [Application](https://discordapp.com/developers/applications/)->Bot->Copy |
| ownerID            | Discord ID of bot owner.                                                                                           |
| commandPrefix      | Command prefix for bot.                                                                                            |
| currencySymbol     | Symbol for server currency.                                                                                        |
| entryChannelID     | ID of channel where information about joining players will appear.                                                 |
| exitChannelID      | ID of channel where information about leaving players will appear.                                                 |
| workChannelID      | ID of channel where information about work will appear.                                                            |
| maxWarnings        | Number of warnings after which user warned user will be kicked out from server.                                    |
| workWaitTime       | Number of seconds before next time when user can use `!work` command.                                              |
| ores               | List of ores that user can get from `*work`.                                                                       |
| autoRolesChannelID | ID of channel with auto-roles.                                                                                     |
| autoRoles          | Groups of roles.                                                                                                   |

### database.json

```json
{
    "users": {
        "ID": {
            "warns"    : 0,
            "money"    : 100,
			"nextWork" : 0
        }
	}
}
```

| Key      | Description                                |
| :------- | :----------------------------------------- |
| warns    | Number of warnings that user have.         |
| money    | Amount of money that user have.            |
| nextWork | Time when user will be able to work again. |

## Authors

* **Michał \<DeBos\> Wróblewski** - Main Developer - [DeBos](https://gitlab.com/DeBos)

## Contact

* Discord: DeBos#3292
* Reddit: [DeBos99](https://www.reddit.com/user/DeBos99)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
